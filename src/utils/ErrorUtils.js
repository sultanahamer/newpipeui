import { all, allPass, pipe, pluck } from "ramda"
import { inLastMinute } from "./DateTimeUtils"

const hasNOrMoreItems = n => arr => arr.length >= n
const allErrorsOccuredInLastMinute = pipe(
    pluck("errorOccuredAt"),
    all(inLastMinute)
)

export const failedInLastMinuteTimes = n => allPass([hasNOrMoreItems(n), allErrorsOccuredInLastMinute])
