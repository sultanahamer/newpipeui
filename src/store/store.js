import Vue from 'vue'
import Vuex from 'vuex';
import prefs from './preferences';
import backend from './backend';
import playlist from './playlist';
import subscriptions from './subscriptions';
import remoteStore from "./remote-store";

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    prefs,
    playlist,
    backend,
    subscriptions,
    remoteStore
  }
})

export default store
